# Minimal makefile for Sphinx documentation
#

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     = .
BUILDDIR      = _build

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

.PHONY: help Makefile

install:
	@pip install -r requirements.txt

livehtml:
	sphinx-autobuild "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
%: Makefile
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

setup:
	sudo apt-get install texlive-xetex texlive-latex-base texlive-extra-utils
	sudo apt-get install texlive-fonts-extra

setup_venv_with_pyenv:
	@echo == to have a 'doc' virtualenv to fit the commited .python-version file
	@echo 0. pyenv virtualenv doc
	@echo 1. restart a terminal
	@echo 2. check "doc" virtualenv is activated
	@echo 3. run "$$ pip install -r requirements.txt"
	@echo 4. create a dir "mkdir ~/.insightface/models" 
	@echo 5. run "$$ wget to download the model 'https://public.db.files.1drv.com/y4meo7xSxBMSwEKCNNefrjPFn0rsge_N3nxwcKpOLCFj_v89l2tLZpqWIK1VquQQLSBgJj6aEPtCJLKwr3f-qYDqiOnkG0DWkpA1h3gRYYSeU5_u8Jw3mGNcZflJrN-J756VtYUn_TlGAd-rd9svBnnI-ernz9ivTp2KoK9ld6zbO7EFEOyziN0xOABCJ03vScHRSvnG4CoKrclzqGSJTILNJWSRRmXUyag4qJipr4jrz0?AVOverride=1' -O inswapper_128.onnx"
	@echo 6. move the file to the folder "$$ mv inswapper_128.onnx ~/.insightface/models"

clean:
	@echo "Delete _build directory"
	rm -r "$(BUILDDIR)"
